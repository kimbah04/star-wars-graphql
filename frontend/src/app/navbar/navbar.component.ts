import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router, public authService: AuthService) { }

  ngOnInit(): void {
  }
logout()
      {
        console.log("logout .......")
        localStorage.clear()
        this.router.navigate(['/']);
        this.authService.isLoggedIn = false;
      }
}
