import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error: string = ""
  private _isLoggedIn$ = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this._isLoggedIn$.asObservable();
  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router, public authService: AuthService) {   }

  myForm: FormGroup = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]]
  })

  ngOnInit(): void {
  }
 async submitForm(formData: any)
   {
    console.log(formData.username);
    this.http.post<any>('http://localhost:8080/authenticate', { username: formData.username, password: formData.password }).subscribe({next: (data) => {
      this.error = data.error;
      if(this.error === "")
         {
          this._isLoggedIn$.next(true);
          localStorage.setItem('_auth', data.token);
          this.authService.isLoggedIn = true
          this.router.navigate(['/home']);
         }
  },
  error: (err) => {this.error = "Network error occured please retry after a few minutes"; console.log(err)},
  complete: () => console.info('complete')})
   }
}
