import { Component, OnInit } from '@angular/core';
import { People, PeoplesService } from '../people.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  page: number = 1;
  error: string = ""
  next: number = 0;
  peoples: People[] = [];
  showLoading: boolean = false;
  constructor(private peoplesService: PeoplesService) { }

  async ngOnInit(): Promise<void> {
    this.page = localStorage.getItem('page') ? Number(localStorage.getItem('page')): this.page;
    localStorage.setItem('page',this.page+"")
    await this.updatePeople();
  }
  async updatePeople() {
    this.showLoading = true
    const result = await this.peoplesService.getPeoples(this.page);
    if(result.next === 0)
       {
        this.page = 0
        localStorage.setItem('page',this.page+"")
        this.error = "Network error occured please retry after a few minutes";
       }
     else
       {
        this.page = this.page === 0?1:this.page
        localStorage.setItem('page',this.page+"")
       }  
    this.next = result.next;
    this.peoples = result.peoples;
    this.showLoading = false
  }
  showPrevious() {
    return this.page > 1 && this.page < 10;
  }

  showNext() {
    return this.page < 9 && this.page > 0;
  }

  async onPrevious() {
    this.page -= 1;
    localStorage.setItem('page',this.page+"");
    await this.updatePeople();
  }

  async onNext() {
    this.page += 1;
    localStorage.setItem('page',this.page+"");
    await this.updatePeople();
  }
}
