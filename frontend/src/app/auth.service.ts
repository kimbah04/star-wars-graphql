import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
  })
export class AuthService {

    private _isLoggedIn$ = new BehaviorSubject<boolean>(false);
    isLoggedIn$ = this._isLoggedIn$.asObservable();
    public isLoggedIn: boolean = false;
  
    constructor() {
      const token = localStorage.getItem('_auth');
      this._isLoggedIn$.next(!!token);
      this.isLoggedIn = (!!token);
    }


}