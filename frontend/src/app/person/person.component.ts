import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { People, PeoplesService } from '../people.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {
  person!: People;
  showLoading: boolean = false;
  constructor(private route: ActivatedRoute, private peoplesService: PeoplesService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(async (params) => {
      this.showLoading = true
      this.person = await this.peoplesService.findPerson(params['name']);
      this.showLoading = false
    });
  }

}
