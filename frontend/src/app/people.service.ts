import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';

export interface People {
  name: string;
  height: string;
  mass: string;
  gender: string;
  homeworld: string;
}



export interface PeoplesResult {
  next: number;
  peoples: People[];
}



@Injectable({
  providedIn: 'root'
})
export class PeoplesService {

  private PeoplesQuery: QueryRef<{peoples: PeoplesResult}, { page: number}>;
  private findPeopleQuery: QueryRef<{personByName: People}, { name: string}>;

  constructor(private apollo: Apollo) { 
    this.PeoplesQuery = this.apollo.watchQuery({
      query: gql`query  peoples($page: Int!){
        peoples(page: $page)
        {
        next 
        peoples { 
          name
          height
          mass
          gender  } 
        }
      }`
    });

    this.findPeopleQuery = this.apollo.watchQuery({
      query: gql`query personByName($name: String!) {
        personByName(name: $name) {
          name
          height
          mass
          gender
          homeworld
        }
      }`
    });

   
  }

  async getPeoples(page: number): Promise<PeoplesResult> {
    const result = await this.PeoplesQuery.refetch({ page });
    return result.data.peoples;
  }

  async findPerson(name: string): Promise<People> {
    const result = await this.findPeopleQuery.refetch({ name });
    return result.data.personByName;
  }

 
}