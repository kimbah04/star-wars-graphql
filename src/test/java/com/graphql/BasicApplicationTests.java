package com.graphql;

import com.graphql.model.People;
import com.graphql.model.PeopleList;
import com.graphql.service.PeopleService;
import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import static org.springframework.test.util.AssertionErrors.*;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BasicApplicationTests {
   @Autowired 
   private PeopleService peopleService; 
     private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private final String PEOPLE_TEST_NAME = "Alderaan";

      
 @Test
 public void getPeoples()
     {
     PeopleList p = peopleService.getAll(0);
      assertNotNull("The list should not be null should return a list",
                p);
     }
@Test
    public void getByName() throws Exception {
        People p = peopleService.getPeople(PEOPLE_TEST_NAME);

         assertNotNull("The People object should not be null should return an Object",
                p);
    }
}
