/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql;

import com.graphql.service.PlanetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author kinah
 */
@RequiredArgsConstructor
@Component
@Slf4j
public class PlanetLoader  implements CommandLineRunner {
    @Autowired
PlanetService planetService;
    @Override
    public void run(String... args) throws Exception {
     //   planetService.loadPlanets();
    }
}
