/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.controller;

import com.graphql.model.People;
import com.graphql.model.PeopleList;
import com.graphql.service.PeopleService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author kinah
 */
@Component
public class QueryResolver implements GraphQLQueryResolver {
 @Autowired
 private PeopleService peopleService;

   
    public PeopleList peoples( int page)
       {
        page = page < 1 || page > 9? 1: page;
       return peopleService.getAll(page);
       }
    
     public People personByName( String name)
       {
       return peopleService.getPeople(name);
       }
}
