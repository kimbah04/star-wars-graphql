/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.controller;

import com.graphql.model.User;
import com.graphql.model.UserLogin;
import com.graphql.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kinah
 */
@RestController
@CrossOrigin
public class LoginController {
    @Autowired
    SecurityService securityService;
     @PostMapping("/authenticate")
    public UserLogin createAuthenticationToken(@RequestBody User login) {
        return securityService.autologin(login.getUsername(), login.getPassword());
    }
}
