/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kinah
 */
@Data
@NoArgsConstructor
public class User {
    
  private String username;
  private String password;
  private String token;  

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
