/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author kinah
 */
@Data
@NoArgsConstructor
public class PeopleList {
    private int next;
    private List<People> peoples;

    public PeopleList(ResponseResult result) {
       this.next = result.getNext() != null ?Integer.parseInt(result.getNext().replace("https://swapi.dev/api/people/?page=", "")):1;
       this.peoples = result.getResults().isEmpty()? new ArrayList<>():result.getResults();
    }
    
}
