/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.model;

import lombok.Data;

/**
 *
 * @author kinah
 */
@Data
public class Planet {
    private String name;
}
