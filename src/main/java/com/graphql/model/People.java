/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.model;

import lombok.Data;

/**
 *
 * @author kinah
 */
@Data
public class People {
 private String name; 
 private String height; 
 private String mass;
 private String gender; 
 private String homeworld;   
}
