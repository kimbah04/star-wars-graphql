/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.model;

import java.util.List;
import lombok.Data;

/**
 *
 * @author kinah
 */
@Data
public class ResponseResult {
 private int count;
 private String next;
 private String previous;
 private List<People> results;
}
