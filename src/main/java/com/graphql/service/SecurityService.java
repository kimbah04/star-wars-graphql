/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.service;

import com.graphql.security.JwtTokenUtil;
import com.graphql.model.User;
import com.graphql.model.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kinah
 */
@Service
public class SecurityService {
    @Autowired
    UserService userService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
     public UserLogin autologin(String username, String password) {
        User user = userService.findByUsername(username);
        UserLogin userLogin = new UserLogin();
        userLogin.setError("");
        if (user == null) {           
            userLogin.setError("Error your login credentials are incorrect");
        } else {
                if (userService.encryptPassword(password, user.getPassword())) {
                    final String token = jwtTokenUtil.generateToken(username);
                    userLogin.setUsername(username);
                    userLogin.setToken(token);
                } else {
                    userLogin.setError("Error your login credentials are incorrect");
                }
        }
        return userLogin;
    }
}
