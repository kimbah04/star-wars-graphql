/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.service;

import com.graphql.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author kinah
 */
@Service
public class UserService {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    User findByUsername(String username) {
       if(username != null && username.equals("yoda"))
          {
          return new User(username, "$2a$10$BqP4eweIJTNQl7ONOp3FTeCvuLzfc8/8OECNkbY0sYOsIT8XRQrx6");
          }
       return null;
    }
    public boolean encryptPassword(String password, String encodedPassword) {
        return bCryptPasswordEncoder.matches(password, encodedPassword);
    }
    
   
    
}
