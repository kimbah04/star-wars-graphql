/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.graphql.service;

import com.google.gson.Gson;
import com.graphql.model.People;
import com.graphql.model.PeopleList;
import com.graphql.model.ResponseResult;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kinah
 */
@Service
@Slf4j
public class PeopleService {
    @Autowired
PlanetService planetService;
private String url = "https://swapi.dev/api/people/";
    public PeopleList getAll(int page) {
        ResponseResult result = null;
        String data = this.getData(url+"?page="+page);
        if(data != null)
             {
                log.info(data);
                result = new Gson().fromJson(data, ResponseResult.class);
               /* result.getResults().forEach(line->{
                line.setHomeworld(planetService.getPlanet(line.getHomeworld()));
                });*/
             }
        return result == null?new PeopleList():new PeopleList(result);
    }

    public People getPeople(String name) {
         ResponseResult result = null;
        String data = this.getData(url+"?search="+name);
        if(data != null)
             {
                result = new Gson().fromJson(data, ResponseResult.class); 
                if(result != null && !result.getResults().isEmpty())
                {
                result.getResults().get(0).setHomeworld(planetService.getPlanet(result.getResults().get(0).getHomeworld()));
                }
             }
        return result == null || result.getResults().isEmpty()?new People():result.getResults().get(0);
    }
    
    
    private String getData(String url)
       {
       String data = null;
        try {
            OkHttpClient client = this.getUnsafeOkHttpClient();
            if(client != null)
                {
                    Request request = new Request.Builder()
                            .url(url)
                            .method("GET", null)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Accept", "application/json")
                            .build();
                    Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        data = response.body().string();
                    }
                }
        } catch (IOException e) {
           log.info(e.getMessage());
        }
        return data;
       }

    private OkHttpClient getUnsafeOkHttpClient() {
         try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            log.info(e.getMessage());
        }
         return null;
    }
}
