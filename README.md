Prerequisites:
Install Java 17 and the latest version of Maven.
You may need to set your JAVA_HOME.
Internet connection for the backend to connect to SWAPI


Running the backend:

cd into the project directory

$ mvn clean install

$java -jar target/graphql-0.0.1.jar

The Angular Frontend is the frontend folder.

Open the README file inside the frontend folder to run the frontend application
